import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.Application;
import page_objects.CategoryOnePage;
import utils.BaseClass;

import java.time.Duration;

public class PositiveCategoryOnePageTests extends BaseClass {
    @Test
    public void firstSubcategoryCheckLinkTest(){
        CategoryOnePage categoryOne = new Application(driver).navigateToCategoryBathPage();
        categoryOne.firstSubcategoryLinkClick();
        Assert.assertEquals("Obiecte sanitare - Magazin online cu Branduri premium | Neakaisa.ro",driver.getTitle());
    }
    @Test
    public void CategoryOneFilterIsAppliedTest(){
        CategoryOnePage categoryOne = new Application(driver).navigateToCategoryBathPage();
        categoryOne.firstCategoryFilterOnClick();
        Assert.assertEquals("Mobilier baie & Obiecte sanitare baie moderne Alcadrain | Neakaisa.ro",driver.getTitle());
    }
    @Test
    public void CategoryOneFilterIsRemovedTest(){
        CategoryOnePage categoryOne = new Application(driver).navigateToCategoryBathPage();
        categoryOne.firstCategoryFilterOnClick();
        categoryOne.firstCategoryRemoveFilterOnClick();
        Assert.assertEquals("Mobilier baie & Obiecte sanitare baie moderne | Neakaisa.ro",driver.getTitle());
    }
    @Test
    public void applySortingInAscendentOrderTest() {
        CategoryOnePage categoryOne = new Application(driver).navigateToCategoryBathPage();
        categoryOne.firstSortingOptionSelect();
        Assert.assertEquals("http://bianca.neakaisa.ro/produse-baie/ordonare-pret/ascendent/",driver.getCurrentUrl());
    }
    @Test
    public void changeNumberOfResultsOnPageTest() {
        CategoryOnePage categoryOne = new Application(driver).navigateToCategoryBathPage();
        categoryOne.changeNumberOfResultsOnPageUponSelect();
        Assert.assertEquals("http://bianca.neakaisa.ro/produse-baie/pppage/90/",driver.getCurrentUrl());
    }
    @Test
    public void clickingOnTheFirstProductOnCategoryPageRedirectsToProductPageTest() {
        CategoryOnePage categoryOne = new Application(driver).navigateToCategoryBathPage();
        categoryOne.firstProductOnCategoryLinkClick();
        Assert.assertEquals("Capac wc slim softclose Ideal Standard Tesi T352701 | Neakaisa.ro",driver.getTitle());
    }
    @Test
    public void addingToCartTheFirstItemOnCategoryPageTest()  {
        CategoryOnePage categoryOne = new Application(driver).navigateToCategoryBathPage();
        categoryOne.firstProductOnPageAddToCartOnClick();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//body/div[@id='added_product']/div[@id='ap-box-product']/a[1]"))));
        categoryOne.viewCartOnClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/one-page-checkout/",driver.getCurrentUrl());
    }
}


