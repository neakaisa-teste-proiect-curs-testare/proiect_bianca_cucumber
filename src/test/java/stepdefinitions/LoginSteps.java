package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page_objects.Application;
import page_objects.LoginPageFE;

import static stepdefinitions.Hooks.driver;

public class LoginSteps {
    LoginPageFE login;

    @Given("Be on the login page")
    public void openLoginPage() {
        System.out.println("Be on the login page");
        login = new Application(driver).navigateToLoginPage();

    }

    @When("Put email {string}")
    public void putEmail(String email) {
        System.out.println("Fill email");
        login.fillEmail(email);
    }

    @And("Put password {string}")
    public void putPassword(String password) {
        System.out.println("Fill password");
        login.fillPassword(password);
    }

    @And("Click on Submit")
    public void clickOnSubmit() {
        login.submit();
    }

    @Then("User is redirected to his account")
    public void userIsRedirectedToHisAccount() {
        Assert.assertEquals("Contul meu | Neakaisa.ro", driver.getTitle());

    }

    @And("User is greeted when is redirected to his account")
    public void userIsGreetedWhenIsRedirectedToHisAccount() {
    Assert.assertTrue(login.checkTheGreetingMessageFromMyAccountIsDisplayed());
            }

    @Then("User is not redirected to his account")
    public void userIsNotRedirectedToHisAccount() {
        Assert.assertEquals("Autentificare | Neakaisa.ro", driver.getTitle());

    }

}



