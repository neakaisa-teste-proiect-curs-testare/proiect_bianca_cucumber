import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.Application;
import page_objects.ProductPage;
import utils.BaseClass;

import java.time.Duration;

public class PositiveProductPageTests extends BaseClass {
    @Test
    public void clickOnLastBreadcrumbLinkTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.lastBreadcrumbClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/capace-wc/filtre/producator/ideal-standard/colectie/tesi/",driver.getCurrentUrl());
    }

    @Test
    public void addProductToWishlistTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.addProductToWishlistOnClick();
        productPage.redirectToWishlistOnClick();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[class=\"p-box-remove-from-wishlist\"]")));
        Assert.assertTrue(productPage.checkIfRemoveProductFromWishlistButtonIsDisplayed());
    }
    @Test
    public void addProductToCartTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.addProductToCartOnClick();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//body/div[@id='added_product']/div[@id='ap-box-product']/a[1]"))));
        productPage.viewCartOnClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/one-page-checkout/",driver.getCurrentUrl());
    }
    @Test
    public void viewProductDescriptionTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.viewProductDescriptionOnClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/capac-wc-ideal-standard-tesi-slim-cu-inchidere-lenta.html#product-view-product-description-title",driver.getCurrentUrl());
    }
    @Test
    public void viewProductAttributesTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.viewProductAttributesOnClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/capac-wc-ideal-standard-tesi-slim-cu-inchidere-lenta.html#product-view-master-attributes-title",driver.getCurrentUrl());
    }
    @Test
    public void viewProductBrandDescriptionTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.viewProductBrandDescriptionOnClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/capac-wc-ideal-standard-tesi-slim-cu-inchidere-lenta.html#product-brand-description",driver.getCurrentUrl());
    }
    @Test
    public void viewProductReviewsTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.viewProductReviewsOnClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/capac-wc-ideal-standard-tesi-slim-cu-inchidere-lenta.html#product-view-reviews-title",driver.getCurrentUrl());
    }
    @Test
    public void addProductReviewModalOpensTest() {

        ProductPage productPage = new Application(driver).navigateToProductPage();
        productPage.addProductReviewOnClick();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[type=\"submit\"][class=\"btn btn-reversed\"]")));
        Assert.assertTrue(productPage.checkIfAddProductReviewModalIsOpenedOnClick());

    }
}