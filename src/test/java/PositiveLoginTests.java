import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import page_objects.Application;
import page_objects.LoginPageFE;
import utils.BaseClass;

public class PositiveLoginTests extends BaseClass{

    @Test
       public void validUsernameAndPasswordTest() {
        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.fillEmail("danalexandrescu2608@gmail.com");
        login.fillPassword("bianca");
        login.submit();

        Assert.assertEquals("Contul meu | Neakaisa.ro",driver.getTitle());
    }
    @Test
    public void greetingMessageInMyAccountPageIsDisplayedTest() {

        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.fillEmail("danalexandrescu2608@gmail.com");
        login.fillPassword("bianca");
        login.submit();
        Assert.assertTrue(login.checkTheGreetingMessageFromMyAccountIsDisplayed());

    }

    @Test
    public void forgottenPasswordLinkIsCorrectTest() {

        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.forgottenPasswordClick();
        Assert.assertEquals("Mi-am uitat parola | Neakaisa.ro",driver.getTitle());

    }

    @Test
    public void goToRegisterPageLinkIsCorrectTest() {

        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.goToRegisterPageOnClick();
        Assert.assertEquals("Cont nou | Neakaisa.ro",driver.getTitle());

    }
}
