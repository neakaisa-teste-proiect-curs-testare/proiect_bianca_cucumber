package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageFE {
        private WebDriver driver;
        private By emailInput = By.id("email");
        private By passwordInput = By.id("parola");

        private By loginButton = By.xpath("//*[@id=\"login_form\"]/div[3]/div/button");

        private By forgottenPassword = By.linkText("Parolă uitată?");

        private By newClient = By.linkText("Client nou");
        private By personalAccountMessage = By.cssSelector("li.user-options-title");

    public LoginPageFE(WebDriver driver) {
        // TODO Auto-generated constructor stub
        this.driver = driver;
    }

    public void fillEmail(String email) {
        driver.findElement(emailInput).sendKeys(email);
    }

     public void fillPassword(String password){
         driver.findElement(passwordInput).sendKeys(password);
        }

        public void submit(){
        driver.findElement(loginButton).click();

    }
    public void forgottenPasswordClick(){
        driver.findElement(forgottenPassword).click();
    }

    public void goToRegisterPageOnClick(){
        driver.findElement(newClient).click();
    }
    public boolean checkTheGreetingMessageFromMyAccountIsDisplayed(){
        return driver.findElement(personalAccountMessage).isDisplayed();

    }
}
