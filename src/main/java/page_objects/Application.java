package page_objects;

    import org.openqa.selenium.WebDriver;

    public class Application {

        public WebDriver driver;

        public Application(WebDriver driver) {
            this.driver = driver;
        }

          //PENTRU PROIECT, LA FINAL, DE LASAT LINKUL CU NEAKAISA, NU bianca.neakaisa, CARE FCT DOAR PE LOCAL!!!!
        public LoginPageFE navigateToLoginPage() {
         //   driver.get("https://www.neakaisa.ro/index.php5?module=CustomerFrontend&action=login&language_code=ro");
            driver.get("http://bianca.neakaisa.ro/index.php5?module=CustomerFrontend&action=login");
            return new LoginPageFE(driver);
        }

        public RegisterPage navigateToRegisterPage() {
           // driver.get("https://www.neakaisa.ro/cont-nou/");
           driver.get("http://bianca.neakaisa.ro/cont-nou/");
            return new RegisterPage(driver);
        }
        public HeaderPage navigateToHeaderPage(){
           // driver.get("https://www.neakaisa.ro/");
            driver.get("http://bianca.neakaisa.ro/");
            return new HeaderPage(driver);
        }

        public CategoryOnePage navigateToCategoryBathPage(){
            //driver.get("https://www.neakaisa.ro/produse-baie/");
            driver.get("http://bianca.neakaisa.ro/produse-baie/");
            return new CategoryOnePage(driver);
        }
        public ProductPage navigateToProductPage(){
            //driver.get("https://www.neakaisa.ro/capac-wc-ideal-standard-tesi-slim-cu-inchidere-lenta.html");
            driver.get("http://bianca.neakaisa.ro/capac-wc-ideal-standard-tesi-slim-cu-inchidere-lenta.html");
            return new ProductPage(driver);
        }
    }



