package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class RegisterPage {
    private WebDriver driver;
    private By surnameInput = By.id("newAccount-customer_firstname");
    private By nameInput = By.id("newAccount-customer_lastname");
    private By phoneInput = By.id("newAccount-customer_phone");
    private By addressInput = By.id("newAccount-customer_street_address");
    private By passwordInput = By.id("newAccount-customer_password");
    private By confirmPasswordInput = By.id("newAccount-customer_confirm_password");

   private By countyInput = By.id("county_idundefined");
   // private By countySelect = By.cssSelector("select[name=\"combo_county_id\"]");
    private By cityInput = By.id("city_idundefined");
    private By emailInput = By.id("newAccount-customer_email");
    private By termsAndConditionsCheckbox=  By.xpath("(//label[@class='checkbox'])[2]");
    private By termsAndConditionsLink =  By.cssSelector("a[data-target=\"#termeni-si-conditii\"]");
    private By termsAndConditionsModal = By.cssSelector("h4[class=\"modal-title\"]");
    private By termsAndConditionsModalCloseButton = By.cssSelector("button[data-dismiss=\"modal\"]");
    private By submitButton = By.xpath("//button[contains(text(),'Trimite')]");
    private By accountAlreadyExistsAlert = By.cssSelector("div[role=\"alert\"]");
    private By mandatoryFieldsAreNotFilledAlert = By.cssSelector("div[role=\"dialog\"][aria-describedby=\"dialogAlert\"]");

    public RegisterPage(WebDriver driver){
        this.driver=driver;
    }

    public void fillSurnameInput(String surname) {
        driver.findElement(surnameInput).sendKeys(surname);
    }

    public void fillNameInput(String name) {
        driver.findElement(nameInput).sendKeys(name);
    }
    public void fillPhoneInput(String phone) {
        driver.findElement(phoneInput).sendKeys(phone);
    }
    public void fillAddressInput(String address) {
        driver.findElement(addressInput).sendKeys(address);
    }
    public void fillPasswordInput(String password) {
        driver.findElement(passwordInput).sendKeys(password);
    }
    public void fillConfirmPasswordInput(String confirmPassword) {
        driver.findElement(confirmPasswordInput).sendKeys(confirmPassword);
    }
    public void fillCountyInput(String county) {
        //Select countyFirstOption = new Select(driver.findElement(countyInput));
        //countyFirstOption.selectByVisibleText("Arad");
        driver.findElement(countyInput).sendKeys(county, Keys.ENTER);
    }
    public void fillCityInput(String city) {
        driver.findElement(cityInput).sendKeys(city, Keys.ENTER);
    }
    public void fillEmailInput(String email) {
        driver.findElement(emailInput).sendKeys(email);
    }
    public void checkboxTermsAndConditionsCheck() {
        driver.findElement(termsAndConditionsCheckbox).click();
    }

    public void linkTermsAndConditionsClick() {
        driver.findElement(termsAndConditionsLink).click();
    }
    public void modalTermsAndConditionsCheckPresence() {
        driver.findElement(termsAndConditionsModal).getText();
    }
    public void registerFormSubmit() {
        driver.findElement(submitButton).click();
    }
    public boolean termsAndConditionsModalCloseButtonIsPresent() {
       return driver.findElement(termsAndConditionsModalCloseButton).isEnabled();
    }
    public boolean accountAlreadyExistsAlertIsPresent(){
        return driver.findElement(accountAlreadyExistsAlert).isDisplayed();
    }
    public boolean mandatoryFieldsAreNotFilledAlertIsDisplayed(){
        return driver.findElement(mandatoryFieldsAreNotFilledAlert).isDisplayed();
    }
}
