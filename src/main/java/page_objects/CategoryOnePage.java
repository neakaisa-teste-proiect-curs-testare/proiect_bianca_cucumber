package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CategoryOnePage {
    private WebDriver driver;
    private By firstSubcategoryLink = By.cssSelector("a[title=\"Obiecte sanitare\"]");
    private By categoryFilter = By.cssSelector("a[title=\"Alcadrain\"]");
    private By categoryRemoveFilter = By.cssSelector("div[class=\"cancel-all-filters\"]");
    private By sortingSelect = By.id("sort_products");
    private By changeNumberOfResultsOnPageSelect = By.cssSelector("select[onchange=\"window.location=setUrlParam('pppage', this.value)\"]");
    private By firstProductOnCategoryLink = By.cssSelector("a[title=\"Capac wc slim softclose Ideal Standard Tesi\"]");
    private By addToCartButtonForTheFirstProduct = By.cssSelector("button[title=\"Adauga in cos\"]");
    private By viewCartButton= By.xpath("//body/div[@id='added_product']/div[@id='ap-box-product']/a[1]");

    public CategoryOnePage(WebDriver driver){
        this.driver=driver;
    }

    public void firstSubcategoryLinkClick(){
        driver.findElement(firstSubcategoryLink).click();
    }
    public void firstCategoryFilterOnClick(){
        driver.findElement(categoryFilter).click();
    }
    public void firstCategoryRemoveFilterOnClick(){
        driver.findElement(categoryRemoveFilter).click();
    }
    public void firstSortingOptionSelect(){
         Select sortingFirstOption = new Select(driver.findElement(sortingSelect));
          sortingFirstOption.selectByVisibleText("Preț ascendent");
    }
    public void changeNumberOfResultsOnPageUponSelect(){
        Select changeNumberOfResultsToTheSecondOption = new Select(driver.findElement(changeNumberOfResultsOnPageSelect));
        changeNumberOfResultsToTheSecondOption.selectByVisibleText("90");
    }
    public void firstProductOnCategoryLinkClick(){
        driver.findElement(firstProductOnCategoryLink).click();
    }
    public void firstProductOnPageAddToCartOnClick(){
        driver.findElement(addToCartButtonForTheFirstProduct).click();
    }
    public void viewCartOnClick(){
        driver.findElement(viewCartButton).click();
    }
}
